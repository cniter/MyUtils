﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace WindowsFormsApplication1
{
    class DBUtils
    {
        private const string connectionString = "Data Source=127.0.0.1;Initial Catalog=TestDB;User ID=test;Password=test;Max Pool Size =512";

        // 查 基函数
        private static DataSet search(string proc_name, SqlParameter[] sql_params, CommandType cmd_type = CommandType.StoredProcedure)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand(proc_name, sqlCon);
            SqlDataAdapter sda = new SqlDataAdapter(sqlCmd);

            sqlCmd.CommandType = cmd_type;  // 设置调用的类型为存储过程
            sqlCmd.Parameters.AddRange(sql_params);

            DataSet ds = new DataSet();
            sda.Fill(ds);   // 查询结果填充数据集
            sda.Dispose();
            ds.Dispose();
            sqlCon.Close();
            sqlCon.Dispose();

            return ds;
        }

        // 增删改 基函数
        private static string update(string proc_name, SqlParameter[] sql_params, CommandType cmd_type = CommandType.StoredProcedure)
        {
            SqlConnection sqlCon = new SqlConnection(connectionString);
            sqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand(proc_name, sqlCon);

            sqlCmd.CommandType = cmd_type;  // 设置调用的类型
            sqlCmd.Parameters.AddRange(sql_params);

            sqlCmd.Parameters.Add(new SqlParameter("@return", SqlDbType.Int));
            sqlCmd.Parameters["@return"].Direction = ParameterDirection.ReturnValue;
            sqlCmd.ExecuteNonQuery();
            string flag = sqlCmd.Parameters["@return"].Value.ToString();

            sqlCon.Close();
            sqlCon.Dispose();

            return flag;
        }

        // 查找 基函数调用方式
        public static DataTable Login(string UserName, string Password) 
        {
            SqlParameter[] sqlParam = new SqlParameter[2];
            sqlParam[0] = new SqlParameter("@UserName", SqlDbType.VarChar, 300);
            sqlParam[0].Direction = ParameterDirection.Input;
            sqlParam[0].Value = UserName.Trim();
            sqlParam[1] = new SqlParameter("@Password", SqlDbType.VarChar, 1000);
            sqlParam[1].Direction = ParameterDirection.Input;
            sqlParam[1].Value = CommonUtils.GetMD5String(Password.Trim());

            DataSet ds = DBUtils.search("getUserInfo", sqlParam);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];

            return dt;
        }

        // 增删改 基函数调用方式
        public static int updateUserPwd(string UserName, string PasswordOld, string PasswordNew)
        {
            SqlParameter[] sqlParam = new SqlParameter[3];
            sqlParam[0] = new SqlParameter("@UserName", SqlDbType.VarChar, 300);
            sqlParam[0].Direction = ParameterDirection.Input;
            sqlParam[0].Value = UserName.Trim();
            sqlParam[1] = new SqlParameter("@PasswordOld", SqlDbType.VarChar, 1000);
            sqlParam[1].Direction = ParameterDirection.Input;
            sqlParam[1].Value = CommonUtils.GetMD5String(PasswordOld.Trim());
            sqlParam[2] = new SqlParameter("@PasswordNew", SqlDbType.VarChar, 1000);
            sqlParam[2].Direction = ParameterDirection.Input;
            sqlParam[2].Value = CommonUtils.GetMD5String(PasswordNew.Trim());

            string flag = DBUtils.update("setUserPwdUpdate", sqlParam);

            if (flag.Equals("0"))
            {
                return 0;   // 修改成功
            }
            else
            {
                return 1;    // 修改失败
            }
        }

        // 查找函数的返回值DataTable绑定DataGridView方式
        // public void LoginTest()
        // {
        //     DataTable dt = DBUtils.Login(user_name.Text, pw.Text);
        //     if (dt.Rows.Count > 0)
        //     {
        //         BindingSource bs = new BindingSource();
        //         bs.DataSource = dt;
        //         dataGridView1.DataSource = bs;
        //     }
        // }
    }   

    class CommonUtils
    {
        public static string GetMD5String(string str)
        {
            MD5 md5 = MD5.Create();
            byte[] data = Encoding.UTF8.GetBytes(str);
            byte[] data2 = md5.ComputeHash(data);

            return GetbyteToString(data2);
            //return BitConverter.ToString(data2).Replace("-", "").ToLower();
        }

        private static string GetbyteToString(byte[] data)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}
