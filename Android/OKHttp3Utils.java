package com.example.admin.test.utils;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by admin on 2018/8/7.
 * okhttp3 前后端数据交互工具类
 * refer to : https://blog.csdn.net/lmj623565791/article/details/47911083
 *            https://www.cnblogs.com/whoislcj/p/5526431.html
 *            https://www.jianshu.com/p/f264922a8a11
 */

public class OKHttp3Utils {
    private static final String TAG = "OKHttp3Utils";
    public static final String root_url = "http://127.0.0.1/Test/TestService.asmx/";  // 请求根目录
    public static final MediaType MEDIA_TYPE_FORM = MediaType.get("application/x-www-form-urlencoded; charset=utf-8");  // 数据是普通表单

    private OkHttpClient _okhttp_client;    // okHttpClient 实例
    private volatile static OKHttp3Utils _okhttp3_utils_instance;   // 单例引用
    private Handler _okhttp_Handler;    // 全局处理子线程和M主线程通信

    private OKHttp3Utils() {
        initOkHttpClient();
        _okhttp_Handler = new Handler(Looper.getMainLooper());
    }

    // 初始化OkHttpClient
    private void initOkHttpClient() {
        _okhttp_client = new OkHttpClient().newBuilder()
                .connectTimeout(10, TimeUnit.SECONDS)  // 设置连接超时时间
                .readTimeout(10, TimeUnit.SECONDS)  // 设置读取超时时间
                .writeTimeout(10, TimeUnit.SECONDS)  // 设置写入超时时间
                .build();
    }

    // 单例化
    public static synchronized OKHttp3Utils getInstance() {
        if (_okhttp3_utils_instance == null) {
            _okhttp3_utils_instance = new OKHttp3Utils();
        }
        return _okhttp3_utils_instance;
    }

    // post异步请求接口
    // action_url：请求地址；post_params：post请求参数
    public static void postAsync(String action_url, HashMap<String, String> post_params, final RequestCallBack callback) {
        String params = getInstance().setPostParams(post_params);
        getInstance()._postAsync(action_url, params, callback);
    }

    // 设置post参数
    private String setPostParams(HashMap<String, String> params_map) {
        StringBuilder temp_params = new StringBuilder();
        try {
            // 处理参数
            int pos = 0;
            for (String key : params_map.keySet()) {
                if (pos > 0) {
                    temp_params.append("&");
                }
                // 对参数进行URLEncoder
                temp_params.append(String.format("%s=%s", key, URLEncoder.encode(params_map.get(key), "utf-8")));
                pos++;
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return temp_params.toString();
    }

    // post异步请求
    private void _postAsync(String action_url, String post_params, final RequestCallBack callback) {
        Request request = buildPostRequest(action_url, post_params);
        requestPostAsyncResult(request, callback);
    }

    // 构造post请求（url及相应的参数）
    private Request buildPostRequest(String action_url, String post_params) {
        RequestBody body = RequestBody.create(MEDIA_TYPE_FORM, post_params);
        return new Request.Builder()
                .url(root_url + action_url)
                .post(body)
                .build();
    }

    // post异步请求结果
    private void requestPostAsyncResult(Request request, final RequestCallBack callback) {
        final Call call = _okhttp_client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                failedCallBack("访问失败", callback);
                Log.e(TAG, e.toString());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String string = response.body().string();
                    string = parseXMLStr(string);
                    Log.e(TAG, "response ----->" + string);
                    successCallBack(string, callback);
                } else {
                    failedCallBack("服务器错误", callback);
                }
            }
        });
    }

    // xml数据解析
    private String parseXMLStr(String xml_str) {
        String json_str = "";
        try {
            // 实例化解析器
            XmlPullParser parser = Xml.newPullParser();
            // 进行解析(参数一：数据源（网络流）；参数二：编码方式)
            parser.setInput(new StringReader(xml_str));
            // 解析标签类型
            int type = parser.getEventType();
            while(type != XmlPullParser.END_DOCUMENT){ // 判断不是结束标签
                switch (type) {
                    case XmlPullParser.START_TAG:
                        // 获取开始标签的名字
                        String start_tag_name = parser.getName();
                        if("string".equals(start_tag_name)){
                            // 获取属性值id的值
//                            String sid = parser.getAttributeName(0);
//                            Log.i("test",""+sid);
                            json_str = parser.nextText();   // 获取当前标签内的文本
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                }
                // 获取下一个标签
                type = parser.next();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        return json_str;
    }


    /**
     * 统一为请求添加头信息
     * @return
     */
    private Request.Builder addHeaders() {
        Request.Builder builder = new Request.Builder()
                .addHeader("Connection", "keep-alive")
                .addHeader("platform", "2")
                .addHeader("phoneModel", Build.MODEL)
                .addHeader("systemVersion", Build.VERSION.RELEASE)
                .addHeader("appVersion", "3.2.0");
        return builder;
    }

    public interface RequestCallBack<T> {
        /**
         * 响应成功
         */
        void onRequestSuccess(T result);

        /**
         * 响应失败
         */
        void onRequestFailed(String errorMsg);
    }

    /**
     * 统一同意处理成功信息
     * @param result
     * @param callBack
     * @param <T>
     */
    private <T> void successCallBack(final T result, final RequestCallBack<T> callBack) {
        _okhttp_Handler.post(new Runnable() {
            @Override
            public void run() {
                if (callBack != null) {
                    callBack.onRequestSuccess(result);
                }
            }
        });
    }

    /**
     * 统一处理失败信息
     * @param errorMsg
     * @param callBack
     * @param <T>
     */
    private <T> void failedCallBack(final String errorMsg, final RequestCallBack<T> callBack) {
        _okhttp_Handler.post(new Runnable() {
            @Override
            public void run() {
                if (callBack != null) {
                    callBack.onRequestFailed(errorMsg);
                }
            }
        });
    }


    public static void postAsyncTest(final Context context) {
        HashMap<String, String> params_map = new HashMap<String, String>();
        params_map.put("s", "&……%*（&（*（%（……HGK");   // 安全参数
        params_map.put("user_name", "张三"); // 用户名参数
        OKHttp3Utils.getInstance().postAsync("getUserInfo", params_map, new OKHttp3Utils.RequestCallBack<String>() {
            @Override
            public void onRequestSuccess(String result)
            {
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestFailed(String errorMsg)
            {
                Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * 具体调用方法可参考：public static void postAsyncTest(final Context context) 函数，
     * 在需要调用的地方将该函数内容复制并修改请求地址和参数即可。
     */

}
