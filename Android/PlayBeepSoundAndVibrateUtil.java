package com.example.admin.test.activity;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;

import com.example.admin.test.R;

import java.io.IOException;

/**
 * Created by admin on 2018/8/29.
 * 描述：蜂鸣和震动提示工具类
 * ※注：在调用该类之前，需要震动权限android.permission.VIBRATE
 *      和将蜂鸣声文件beep.ogg放入res文件里的raw文件夹中
 */

public class PlayBeepSoundAndVibrateUtil {
    private static final String TAG = "PlayBeepSoundAndVibrateUtil";
    private static final long VIBRATE_DURATION = 200L;
    private static final float BEEP_VOLUME = 0.10f;
    private MediaPlayer _media_player;
    /** 是否播放声音 */
    private boolean _is_play_beep = true;
    /** 是否震动 */
    private boolean _is_vibrate = true;
    private Activity _context;

    PlayBeepSoundAndVibrateUtil(Activity context) {
        this._context = context;
    }


    private final MediaPlayer.OnCompletionListener beepListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.seekTo(0);
        }
    };

    /**
     * 初始化声音资源
     */
    private void initBeepSound() {
        // 如果要播放声音并且没有播放器时
        if (_is_play_beep && _media_player == null) {
            // 设置声道流格式为音乐
            _context.setVolumeControlStream(AudioManager.STREAM_MUSIC);
            _media_player = new MediaPlayer();
            _media_player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            // 设置声音完成后监听
            _media_player.setOnCompletionListener(beepListener);
            // 设定数据源，并准备播放
            AssetFileDescriptor file = _context.getResources().openRawResourceFd(R.raw.beep);    //*注： 将蜂鸣声文件beep.ogg放入res文件里的raw文件夹中
            try {
                _media_player.setDataSource(file.getFileDescriptor(), file.getStartOffset(), file.getLength());
                file.close();
                _media_player.setVolume(BEEP_VOLUME, BEEP_VOLUME);// 设置音量
                _media_player.prepare();
            } catch (IOException e) {
                _media_player = null;
            }
        }
    }

    public void setIsPlayBeep(boolean is_play_beep) {
         this._is_play_beep = is_play_beep;
    }

    public void setIsVibrate(boolean is_vibrate) {
        this._is_vibrate = is_vibrate;
    }

    /**
     * 响铃和震动
     */
     public void playBeepSoundAndVibrate() {
        AudioManager audioService = (AudioManager) _context.getSystemService(Context.AUDIO_SERVICE);
        // 如果当前是铃音模式，则继续准备下面的 蜂鸣提示音操作，如果是静音或者震动模式。就不要继续了。因为用户选择了无声的模式，我们就也不要出声了。
        if (audioService.getRingerMode() != AudioManager.RINGER_MODE_NORMAL) {
            _is_play_beep = false;
        }
        initBeepSound();

        if (_is_play_beep && _media_player != null) {
            _media_player.start();
            //mediaPlayer.pause();
        }
        if (_is_vibrate) {
            //*注： 获取系统震动服务,需要震动权限android.permission.VIBRATE
            Vibrator vibrator = (Vibrator) _context.getSystemService(Context.VIBRATOR_SERVICE);
            // 震动一次
            vibrator.vibrate(VIBRATE_DURATION);
            // 第一个参数，指代一个震动的频率数组。每两个为一组，每组的第一个为等待时间，第二个为震动时间。
            // 比如 [2000,500,100,400],会先等待2000毫秒，震动500，再等待100，震动400
            // 第二个参数，repest指代从 第几个索引（第一个数组参数） 的位置开始循环震动。
            // 会一直保持循环，我们需要用 vibrator.cancel()主动终止
            // vibrator.vibrate(new long[]{300,500},0);
        }
    }

    public void release() {
        if (_media_player != null) {
            _media_player.stop();
            _media_player.release();
            _media_player = null;
        }
    }

    /**
     * 具体调用方法为：
     * 首先在TestActivity类中声明一个变量 private PlayBeepSoundAndVibrateUtil pbsavu;
     * 然后在该类的 onCreate() 方法里创建一个对象 pbsavu = new PlayBeepSoundAndVibrateUtil(TestActivity.this);
     * 接着在需要调用震动和响铃的地方 调用 pbsavu.playBeepSoundAndVibrate();
     * 最后在该类的 onDestroy() 方法中加上 if (pbsavu != null) { pbsavu.release(); pbsavu = null; }
     */
}
